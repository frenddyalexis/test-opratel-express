const express = require('express');

const user = require('../controllers/user.controller');

const router = express.Router();

module.exports = router;

router.post('/', function (req, res, next) {
    res.status(200).send("Bienvenido al test de opratel");
});

//Listar todos los usuarios
router.get("/users", user.getUsers );

//Listar usuario por id
router.get("/users/:id", user.getUser );

//Activar o desactivar usuario
router.put("/users", user.updateActive );

//agregar usuario
router.post("/users", user.addUser );

//Eliminar usuario
router.delete("/users", user.deleteUser )


