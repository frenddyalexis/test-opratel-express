var connection = require('../config/db.myslq.config');

var userModel = {};

//obtenemos todos los usuarios
userModel.getUsers = function (callback) {
    if (connection) {
        connection.query('SELECT a.username,a.password,a.email FROM users as a ORDER BY id', function (error, rows) {
            if (error) {
                throw error;
            } else {
                callback(null, rows);
            }
        });
    }
}

//obtenemos un usuario por su id
userModel.getUser = function (id, callback) {
    if (connection) {
        var consulta = 'SELECT a.username,a.password,a.email  FROM users as a WHERE id = ' + connection.escape(id);
        connection.query(consulta, function (error, row) {
            if (error) {
                throw error;
            } else {
                callback(null, row);
            }
        });
    }
}

//Activar/desactivar usuario
userModel.updateActive = function (userData, callback) {
    if (connection) {
        var sql = 'UPDATE users SET active = ' + connection.escape(userData.active) + ' WHERE id = ' + userData.id;
        console.log(sql);
        connection.query(sql, function (error, result) {
            if (error) {
                throw error;
            }
            else {
                callback(null, { "msg": "success" });
            }
        });
    }
}


//añadir un nuevo usuario
userModel.insertUser = function (userData, callback) {

    if (connection) {
        connection.query('INSERT INTO users SET ?', userData, function (error, result) {
            if (error) {
                throw error;
            }
            else {
                //devolvemos la última id insertada
                callback(null, { "insertId": result.insertId });
            }
        });
    }
}


//eliminar un usuario 
userModel.deleteUser = function (id, callback) {
    if (connection) {
        var sqlExists = 'SELECT * FROM users WHERE id = ' + connection.escape(id);
        connection.query(sqlExists, function (err, row) {
            if (row) {
                var sql = 'DELETE FROM users WHERE id = ' + connection.escape(id);
                connection.query(sql, function (error, result) {
                    if (error) {
                        throw error;
                    }
                    else {
                        callback(null, { "msg": "deleted" });
                    }
                });
            }
            else {
                callback(null, { "msg": "notExist" });
            }
        });
    }
}

//exportamos el objeto para tenerlo disponible en la zona de rutas
module.exports = userModel;