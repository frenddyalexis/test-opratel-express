const mysqlhost = process.env.MYSQLHOST;
const mysqlserver = process.env.MYSQLUSER;
const mysqlpassword = process.env.MYSQLPASSWORD;
const mysqldatabase = process.env.MYSQLDATABASE;


var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : `${mysqlhost}`,
  user     : `${mysqlserver}`,
  password : `${mysqlpassword}`,
  database : `${mysqldatabase}`
});

connection.connect(function(err) {
    if (err) throw err;
});

module.exports = connection;