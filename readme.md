# TEST NODE-EXPRESS-MYSQL-OPRATEL
REST API

## Quick Start

- Clone this repo or download it's release archive and extract it somewhere
- You may delete `.git` folder if you get this code via `git clone`
- Run `npm install`
- you can use the import file in the repository (test_opratel_db.sql)
- Configure your `.env` file for authenticating via database
- Run `npm start`




## ETC

I made a Postman collection [here](https://www.getpostman.com/collections/30c5954560cc0e8375db).
