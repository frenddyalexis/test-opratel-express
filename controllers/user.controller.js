var UserModel = require('../models/users.model');


exports.getUsers = function (req, res) {
    console.log("usuariooos");
    UserModel.getUsers(function (error, data) {
        res.status(200).json(data);
    });
};

exports.getUser = function (req, res) {
    console.log("dsdsd");
    var id = req.params.id;
    console.log(id);
    UserModel.getUser(id, function (error, data) {
        if (typeof data !== 'undefined' && data.length > 0) {
            res.status(200).json(data);
        }
        else {
            res.status(200).json({ "mensaje": "Usuario no existe" });
        }
    });
};

exports.updateActive = function (req, res) {
    var userData = { id: req.body.id, active: req.body.active };
    console.log(userData);

    UserModel.updateActive(userData, function (error, data) {

        if (data && data.msg) {
            res.status(200).json(data);
        }
        else {
            res.status(200).json({ "mensaje": "Error" });
        }
    });
};



exports.addUser = function (req, res) {
    var userData = {
        id: null,
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        active: 1
    };
    UserModel.insertUser(userData, function (error, data) {
        
        if (data && data.insertId) {
            res.redirect("/users/" + data.insertId);
        }
        else {
            res.json(500, { "mensaje": "Error" });
        }
    });
};


exports.deleteUser = function (req, res) {
    var id = req.param('id');
    UserModel.deleteUser(id, function (error, data) {
        if (data && data.msg === "deleted" || data.msg === "notExist") {
            res.json(200, data);
        }
        else {
            res.json(500, { "mensaje": "Error" });
        }
    });
};